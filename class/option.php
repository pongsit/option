<?php

namespace pongsit\option;

class option extends \pongsit\model\model{
	
	public function __construct(){
		parent::__construct();
    }
    
	function from_db($table,$default=-1,$skips=array()){
		if($default == 'last'){
			$query = 'SELECT max(id) as max_id FROM '.$table.';';
			$results = $this->db->query_array0($query);
			$default = $results['max_id'];
		}
		$query = 'SELECT * FROM '.$table.';';
		$option = '';
		if($default == 'dash'){$option .= '<option selected value="">-</option>';}
		
		$results = $this->db->query_array($query);
		
		foreach($results as $k=>$vs){
			if(in_array($vs['id'],$skips)){continue;}
			if(in_array($vs['name'],$skips)){continue;}
			$selected="";
			if($vs['id']==$default){ $selected = 'selected ';}
			$option .= '<option '.$selected.'value="'.$vs['id'].'">'.$vs['name'].'</option>';
		}
		return $option;
	}
	function from_array($arrays,$default=-1){
		$option = '';
		if($default == 'dash'){$option .= '<option selected value="">-</option>';}
		foreach ($arrays as $id=>$name) {
				$option .= '<option value="'.$id.'">'.$name.'</option>';
		}
		return $option;
	}
	function from_model($arrays,$default=-1){
		$option = '';
		if($default == 'dash'){$option .= '<option selected value="">-</option>';}
		foreach ($arrays as $values) {
				$option .= '<option value="'.$values['id'].'">'.$values['name'].'</option>';
		}
		return $option;
	}
	function checkbox($table,$checks,$extra_query=''){
		if(empty($checks)){ $checks = array(); }
		if(empty($extra_query)){
			$query = 'SELECT * FROM '.$table.';';
		}else{
			$query = 'SELECT * FROM '.$table.' '.$extra_query.';';
		}
		$options = array();
		$results = $this->db->query_array($query);

		foreach($results as $rows) {
			$name = $rows['name'];
			$name_show = $rows['name_show'];
			$selected='';
			if(in_array($name, $checks)){ $selected = 'checked ';}
			if(!empty($rows['id'])){
				$options[$rows['id']] = '<span style="white-space: nowrap;"><input type="checkbox" '.$selected.' name="check[]" value="'.$name.'"> '.$name_show.'</span>';
			}else{
				$options[] = '<span style="white-space: nowrap;"><input type="checkbox" '.$selected.' name="check[]" value="'.$name.'"> '.$name_show.'</span>';
			}
		}
		return $options;
	}
	function day_selection($optionals=array()){
		$time = new \pongsit\time\time();
		$option = '';
		$d = $time->now('d');
		if(!empty($optionals['selected'])){
			$d=$optionals['selected'];
		}
		if(in_array('default-dash',$optionals)){$option .= '<option selected value="">-</option>';}
		for($i=1;$i<=31;$i++){
			$selected="";
			if($i<10){$i_show = '0'.$i;}else{$i_show = $i;} 
			if($i == $d && !in_array('default-dash',$optionals)){ $selected = 'selected ';}
			$option .= '<option '.$selected.'value="'.$i_show.'">'.$i_show.'</option>';
		}
		return $option;
	}
	function month_selection($optionals=array()){
		$time = new \pongsit\time\time();
		$option = '';
		$m = $time->now('m');
		if(!empty($optionals['selected'])){
			$m=$optionals['selected'];
		}
		if(in_array('default-dash',$optionals)){$option .= '<option selected value="">-</option>';}
		for($i=1;$i<=12;$i++){
			$selected=""; 
			if($i<10){$i_show = '0'.$i;}else{$i_show = $i;} 
			if($i == $m && !in_array('default-dash',$optionals)){ $selected = 'selected ';}
			$option .= '<option '.$selected.'value="'.$i_show.'">'.$i_show.'</option>';
		}
		return $option;
	}
	function year_selection($optionals=array()){
		$time = new \pongsit\time\time();
		$option = '';
		$y = $time->now('Y');
		if(!empty($optionals['selected'])){
			$y=$optionals['selected'];
		}
		if(in_array('default-dash',$optionals)){$option .= '<option selected value="">-</option>';}
		for($i=2;$i>0;$i--){
			$selected=""; 
			$value = $y-$i;
			$option .= '<option '.$selected.'value="'.$value.'">'.$value.'</option>';
		}
		for($i=0;$i<3;$i++){
			$selected=""; 
			$value = $y+$i;
			if($i == 0 && !in_array('default-dash',$optionals)){ $selected = 'selected ';}
			$option .= '<option '.$selected.'value="'.$value.'">'.$value.'</option>';
		}
		return $option;
	}
	function year_thai_selection($optionals=array()){
		$time = new \pongsit\time\time();
		$option = '';
		$y = $time->now('Y');
		if(!empty($optionals['selected'])){
			$y=$optionals['selected'];
		}
		if(in_array('default-dash',$optionals)){$option .= '<option selected value="">-</option>';}
		for($i=2;$i>0;$i--){
			$selected=""; 
			$value = $y-$i+543;
			$option .= '<option '.$selected.'value="'.$value.'">'.$value.'</option>';
		}
		for($i=0;$i<3;$i++){
			$selected=""; 
			$value = $y+$i+543;
			if($i == 0 && !in_array('default-dash',$optionals)){ $selected = 'selected ';}
			$option .= '<option '.$selected.'value="'.$value.'">'.$value.'</option>';
		}
		return $option;
	}
}